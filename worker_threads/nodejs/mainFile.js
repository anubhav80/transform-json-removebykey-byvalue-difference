'use strict';
const { Worker } = require('worker_threads');
const workerThread = new Worker('./worker.thread.js'); // PATH MUST BE WITH `.js` or `.mjs`

/**
 * @description - This is a way by which you can listen the message came from worker thread
 */
workerThread.on('message', message => console.log(message));

/**
 * @description - This is a way by which you can listen the error came from worker thread
 */
worker.on('error', error => console.error(error));

/**
 * @description - This is a way by which you can listen the exit code came from worker thread
 *
 * @note:- only useful when we wants to perform some action on worker thread exit
 */
worker.on('exit', exitCode => console.info(exitCode));

/**
 * @description - This is a way by which you can pass some data to worker thread for processing
 */
workerThread.postMessage('divyesh');


