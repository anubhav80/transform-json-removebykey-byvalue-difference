'use strict';
const { parentPort } = require('worker_threads');

/**
 * @description - Get the message to worker thread from a file from where it's been pass the data
 */
parentPort.once('message',
	message => sendResponse({name: message}));

/**
 * @description - Responsible to post the message to main thread.
 *
 * @param {Object} jsonObject - The json or any thing you can pass to the main thread
 */
function sendResponse(jsonObject) {
	parentPort.postMessage(jsonObject);
}
