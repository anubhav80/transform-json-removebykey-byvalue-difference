'use strict';
const firebaseAndSequalizeParallelDML  = require('./firebaseAndSequalizeParallelDML');

/**
 * @module utils
 */
module.exports = {
	firebaseAndSequalizeParallelDML
};
